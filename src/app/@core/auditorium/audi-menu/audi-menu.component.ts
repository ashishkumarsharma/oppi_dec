import { Component, EventEmitter, OnInit, Output } from '@angular/core';
declare var $:any;
@Component({
  selector: 'app-audi-menu',
  templateUrl: './audi-menu.component.html',
  styleUrls: ['./audi-menu.component.scss']
})
export class AudiMenuComponent implements OnInit {

  imag:any;
  like = false;
  @Output() myOutput:EventEmitter<any> = new EventEmitter();
  Leftmenu=[
    {name:'clap', img:'assets/icons/clap.png', audio:'assets/mp3/applause.mp3'},
    {name:'Hoot', img:'assets/icons/hoot.png', audio:'assets/mp3/hoot.mp3'},
    {name:'like', img:'assets/icons/like.png', audio:'like'},
    {name:'heart', img:'assets/icons/heart.png', audio:'heart'}
  ];
  Rightmenu=[
    {name:'ask question', img:'assets/icons/ask.png'},
   
    /* {name:'poll', img:'assets/icons/poll.png'}, */
    {name:'group chat', img:'assets/icons/chat-h.png'}
  ];
  constructor() { }

  ngOnInit(): void {
  }

  openRigthMenu(item){
    if(item.includes('question')){
      $("#ask_question_modal").modal("show");
    }
    if(item == 'poll'){
      $("#poll_modal").modal("show");
    }
    if(item == 'group chat'){
      $("#t_one_groupChat").modal("show");
    }
  }
  likeopen(data){
    if(data =='like'){
      this.imag = 'assets/icons/like.png'
    }
    if(data =='assets/mp3/applause.mp3'){
      this.imag = 'assets/icons/clap.png'
    }
    if(data =='heart'){
      this.imag = 'assets/icons/heart.png'
    }
    this.like = true;
    setTimeout(() => {
      this.like = false;
    }, 10000);
  }
  openLeftMenu(item){
    if(item == null) {
      $("#quiz_modal").modal("show");
    } else {
      var sound:any = document.createElement('audio');
      sound.id = 'audio-player';
      sound.src = item
      sound.type = 'audio/mpeg';
      sound.preload = true;
      sound.autoplay = '';
      sound.controls = 'controls';
      sound.play();
      sound.style.display='none';
      document.getElementById('setAudio').appendChild(sound);
    }
  }
}
