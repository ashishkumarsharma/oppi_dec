import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LobbyRoutingModule } from './lobby-routing.module';
import { LobbyComponent } from './lobby.component';
import { HttpClientModule } from '@angular/common/http';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';
import { ReactiveFormsModule } from '@angular/forms';
import { WelcomeChatComponent } from 'src/app/@main/shared/welcome-chat/welcome-chat.component';
import { WelcomeChatModule } from 'src/app/@main/shared/welcome-chat/welcome-chat.module';


@NgModule({
  declarations: [LobbyComponent],
  imports: [
    CommonModule,
    LobbyRoutingModule,
    HttpClientModule,
    PdfmodalModule,ReactiveFormsModule,
    WelcomeChatModule
  ]
})
export class LobbyModule { }
