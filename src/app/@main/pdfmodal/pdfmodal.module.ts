import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PdfmodalComponent } from './pdfmodal.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { WelcomeChatModule } from '../shared/welcome-chat/welcome-chat.module';


@NgModule({
  declarations: [PdfmodalComponent],
  imports: [
    CommonModule, 
    RouterModule,
    FormsModule,  
  ],
  exports:[PdfmodalComponent]
})
export class PdfmodalModule { }
