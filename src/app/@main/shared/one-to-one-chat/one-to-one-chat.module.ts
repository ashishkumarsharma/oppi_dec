import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OneToOneChatComponent } from './one-to-one-chat.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [OneToOneChatComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [OneToOneChatComponent]
})
export class OneToOneChatModule { }
