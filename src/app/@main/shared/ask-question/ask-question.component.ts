import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
// import Swal from 'sweetalert2/dist/sweetalert2.js';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-ask-question',
  templateUrl: './ask-question.component.html',
  styleUrls: ['./ask-question.component.scss']
})
export class AskQuestionComponent implements OnInit {
  audi_id;
  msg;
  textMessage = new FormControl();
  constructor(private _ar: ActivatedRoute, private _ds: DataService) { }

  ngOnInit(): void {
  }
  postQuestion(value){
    this._ar.paramMap.subscribe(params => {
      this.audi_id = params.get('id');
    });
    let data = JSON.parse(localStorage.getItem('virtual'));
    let audi_id = this.audi_id;
    this._ds.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
      if(res.code == 1){
        this.msg = 'Submitted Succesfully';
        Swal.fire({
          position: 'top',
          icon: 'success',
          title: 'Your Question has been Submitted Succesfully!',
          showConfirmButton: false,
          timer: 3000
        });
        $("#ask_question_modal").modal("hide");
      }
      this.textMessage.reset(); 
    }))
  }
  closeModal(){
    $("#ask_question_modal").modal("hide");
  }

}
