import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../services/chat.service';
import { DataService, localService } from '../services/data.service';
import Swal from 'sweetalert2'
import { AuthService } from '../services/auth.service';
import { BriefcaseModalComponent } from './briefcase-modal/briefcase-modal.component';
declare var $:any;
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {

  @ViewChild(BriefcaseModalComponent) private briefcase:BriefcaseModalComponent;

  constructor(private chat: ChatService, private _ls:localService, private _auth:AuthService, public router: Router, private location: Location) { }

  ngOnInit(): void {
    // var arrEl = ['book', 'kick', 'rock', 'book', 'lock']
    // var books = [
    //   { title: "C++", author: "Bjarne" },
    //   { title: "Java", author: "James" },
    //   { title: "Python", author: "Guido" },
    //   { title: "Java", author: "James" },
    //   { title: "angular", author: "James" },
    //   { title: "Java", author: "kkk" },
    // ];
    // let newArray:any = [];
    // let uniqueObj:any = {};
    // books.filter((item, index)=>{
    //   uniqueObj[item.title]=index;
    //   console.log('filter',  uniqueObj);
      
    // });
    // for (let i in uniqueObj) {
    //   newArray.push(books[uniqueObj[i]])
    // }
    // for (const iterator of books) {
      
    // }
    // console.log('arr', newArray);
    
    // arrEl = arrEl.filter( ( item, index, inputArray )=> {
    //   return inputArray.indexOf(item) == index;
    // });
    // console.log('arr', arrEl);

  }
  gotoBack(){
    this.location.back();
  }
  ngAfterViewInit(){    
    this.chat.socketConnection();  
    this.chat.getSocketMessages().subscribe((data:any)=>{
      console.log(data)
      const splitData = data.split('_');
      if(data.includes('custom_live')){
        const Toast = Swal.mixin({
          toast: true,
          position: 'top',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        
        Toast.fire({
          icon: 'success',
          title: 'Auditorium will begin soon'
        });
      }
      setTimeout(() => {
        this.getId(splitData[2]);
      }, 3000);
    }) 
  }
  submitPageAnalytics(item){
    this._ls.stepUpAnalytics(item);
  }
  getId(id){
    this._auth.settingItems$.subscribe(items => {
      if(items.length){
        let path = items[0]["auditorium"].filter(x=>x.id==id)[0]['path'];
        this.router.navigate(['/auditorium/'+path+'/'+id]);        
      }
    });  
  }
  readOutputMenu(item){
    /* alert(item.path)
    if(item.path === 'helpdesk'){
      $('#welcome_chat_modal').modal('show');
    } */
    console.log(item);
    if(item === 'briefcase'){
      this.briefcase.getBriefcaseList();        
    } else{
      
    }
  }
}
